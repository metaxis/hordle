# Changelog

`hordle` uses [PVP Versioning][1].
The changelog is available [on GitHub][2].

## 0.0.3

* Bump up stack's resolver to lts-20.17

## 0.0.2

* Bump up stack's resolver to lts-20.1
* Fix ToUnescapingTF

## 0.0.0

* Initially created.

[1]: https://pvp.haskell.org
[2]: https://github.com/metaxis/hordle/releases
