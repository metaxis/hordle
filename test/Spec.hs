module Main (main) where

import Hordle (projectName)


main :: IO ()
main = putStrLn ("Tests for " ++ projectName)
