{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE NoStarIsType #-}
{-# LANGUAGE StandaloneKindSignatures #-}


module Util
  (
    ushow
  , uprint
  , replicateMVar_
  ) where

import Data.Char (showLitChar)
import Unsafe.Coerce (unsafeCoerce)
import Control.Concurrent.MVar ( readMVar, MVar, swapMVar )
import Debug.Trace ( traceM, traceShowM )
import Control.Monad ( void )


newtype UnescapingChar = UnescapingChar {unescapingChar :: Char}

instance Show UnescapingChar where
  showsPrec :: p -> UnescapingChar -> ShowS
  showsPrec _ (UnescapingChar '\'') = showString "'\\''"
  showsPrec _ (UnescapingChar c)
   = showChar '\''
   . showLitChar' c
   . showChar '\''

  showList :: [UnescapingChar] -> String -> String
  showList cs
    = showChar '"'
    . showLitString' (map unescapingChar cs)
    . showChar '"'

{-
original instance

instance  Show Char  where
    showsPrec _ '\'' = showString "'\\''"
    showsPrec _ c    = showChar '\'' . showLitChar c . showChar '\''

    showList cs = showChar '"' . showLitString cs . showChar '"'
-}


showLitChar' :: Char -> ShowS
showLitChar' c s | c > '\DEL' = showChar c s
showLitChar' c s = showLitChar c s

{-
showLitChar                :: Char -> ShowS
showLitChar c s | c > '\DEL' =  showChar '\\' (protectEsc isDec (shows (ord c)) s)
showLitChar '\DEL'         s =  showString "\\DEL" s
showLitChar '\\'           s =  showString "\\\\" s
showLitChar c s | c >= ' '   =  showChar c s
showLitChar '\a'           s =  showString "\\a" s
showLitChar '\b'           s =  showString "\\b" s
showLitChar '\f'           s =  showString "\\f" s
showLitChar '\n'           s =  showString "\\n" s
showLitChar '\r'           s =  showString "\\r" s
showLitChar '\t'           s =  showString "\\t" s
showLitChar '\v'           s =  showString "\\v" s
showLitChar '\SO'          s =  protectEsc (== 'H') (showString "\\SO") s
showLitChar c              s =  showString ('\\' : asciiTab!!ord c) s
-}


showLitString' :: String -> ShowS
showLitString' []         s = s
showLitString' ('"' : cs) s = showString "\\\"" (showLitString' cs s)
showLitString' (c : cs)   s = showLitChar' c (showLitString' cs s)

{-
showLitString []         s = s
showLitString ('"' : cs) s = showString "\\\"" (showLitString cs s)
showLitString (c   : cs) s = showLitChar c (showLitString cs s)
-}


-- closed typed family
type ToUnescapingTF :: forall k. k -> k
type family ToUnescapingTF (a :: k) :: k where
  ToUnescapingTF Char = UnescapingChar
  ToUnescapingTF (t b) = (ToUnescapingTF t) (ToUnescapingTF b)
  -- ^ The applied type constructor requires recursive processing.
  ToUnescapingTF a = a -- Everything else is unaffected.

-- >>> :kind! ToUnescapingTF Char
-- ToUnescapingTF Char :: Type
-- = UnescapingChar

-- >>> :kind! ToUnescapingTF Int
-- ToUnescapingTF Int :: Type
-- = Int

-- >>> :kind! ToUnescapingTF (Maybe Char)
-- ToUnescapingTF (Maybe Char) :: Type
-- = Maybe UnescapingChar

-- >>> :kind! ToUnescapingTF (Either String)
-- ToUnescapingTF (Either String) :: Type -> Type
-- = Either [UnescapingChar]


class ToUnescaping a where
  toUnescaping :: a -> ToUnescapingTF a

instance ToUnescaping a where
  toUnescaping = unsafeCoerce

type UnescapingShow t = (ToUnescaping t, Show (ToUnescapingTF t))

ushow :: UnescapingShow t => t -> String
ushow = show . toUnescaping

uprint :: UnescapingShow t => t -> IO ()
uprint = putStrLn . ushow

-- in GGHCi try following commands:
-- :set -interactive-print=uprint
-- "Vogt Nyx: »Büß du ja zwölf Qirsch, Kämpe!«"



replicateMVar_ :: MVar Int -> IO a -> IO ()
replicateMVar_ cnt0 f =
    loop cnt0
  where
    loop cnt = do
      counter <- readMVar cnt
      traceM "counter: " >> traceShowM counter
      if counter <= 0
        then traceM "Finish" >> pure ()
        else do
          void $ swapMVar cnt (counter - 1)
          _ <- f
          loop cnt
