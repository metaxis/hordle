{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE DataKinds    #-}
{-# LANGUAGE PolyKinds    #-}
{-# LANGUAGE ViewPatterns #-}



module Hordle
    ( hordle
    , fetchLeftWords
    , getWords
    ) where

import Control.Concurrent.MVar (modifyMVar, modifyMVar_, newMVar, readMVar, swapMVar)
import Control.Monad (void)
import qualified Data.ByteString as B
import Data.IntMap (IntMap)
import qualified Data.IntMap as IntMap
import Data.Maybe (mapMaybe)
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.Encoding as TE
import qualified Data.Text.IO as TIO
import qualified Data.Text.Lines as Line
import Debug.Trace (traceM)
import FixedWord (SomeText, fromSomeText, mkSomeText)
import Numeric.Natural (Natural)
import System.Random (randomRIO)
import Util (replicateMVar_, uprint)


{-
- Make robot to play.
  1. Get full list of words of a language. One word per line from somewhere and save to app root directory.
  3. Set a riddle word and see how a robot guesses it with `hordle`.

- If you are a human and plays against a computer and already made several unlucky attempts, try `fetchLeftWords` to retrieve a list of all possible words are left.

-}

file :: FilePath
file = "singular.txt"

hordle :: Text -> IO ()
hordle riddle = do
  bFile <- B.readFile file
  let allLines = Line.lines $ Line.fromText $ TE.decodeUtf8 bFile
  let n :: Natural = fromIntegral $ T.length riddle
  print n
  let setOfWords = getFixedWords n allLines
  print $ Set.size setOfWords
  !wordsMVar <- newMVar setOfWords
  !matchedMVar <- newMVar IntMap.empty
  !notMatchedMVar <- newMVar IntMap.empty
  !otherMVar <- newMVar Set.empty
  !firedMVar <- newMVar Set.empty
  !countMVar <- newMVar 8


  replicateMVar_ countMVar $ do

    -- modify word set
    !updatedWordSet <- modifyMVar wordsMVar $ \wordSet -> do
      traceM "=========================="
      traceM "Previous limits:"
      matched <- readMVar matchedMVar
      uprint matched
      notMatched <- readMVar notMatchedMVar
      other <- readMVar otherMVar
      uprint other
      fired <- readMVar firedMVar
      uprint fired
      let updated = updateWords
            wordSet
            matched
            notMatched
            other
            fired
      traceM "-------------------------"
      pure (wordSet, updated)
    -- update word set

    -- new index from new word set
    let sizeOfSet = Set.size updatedWordSet
    putStr "Size of set: " >> print sizeOfSet
    randomIndex <- fromEnum <$> randomRIO (0, sizeOfSet-1)
    putStr "Index: " >> print randomIndex
    --  matchedMVar

    -- get new answer from list
    let randomWord = Set.elemAt randomIndex updatedWordSet
    putStr "Random word: " >> TIO.putStrLn (fromSomeText randomWord)

    if fromSomeText randomWord == riddle
      then do
        void $ swapMVar countMVar 0
        putStr "Word has been found: "
        TIO.putStrLn $ fromSomeText randomWord
        print =<< readMVar countMVar
      else do
        -- check intersections
        traceM "-------------------------"
        traceM "New limits: "
        let intersected = checkIntersection (mkIntMap riddle) (mkIntMap $ fromSomeText randomWord)

        -- update matched chars
        let matchedAfter = fst intersected
        modifyMVar_ matchedMVar $ \oldMatched -> do
          let newMatched = IntMap.union matchedAfter oldMatched
          uprint newMatched
          pure newMatched

        -- update other found chars
        let otherAfter = snd intersected
        modifyMVar_ otherMVar $ \oldOther -> do
          let newOther = Set.union otherAfter oldOther
          uprint newOther
          pure newOther

        -- update fired found chars
        let firedAfter = firedChars randomWord intersected
        modifyMVar_ firedMVar $ \oldFired -> do
          let newFired = Set.union firedAfter oldFired
          uprint newFired
          pure newFired
        traceM "=========================="

updateWords :: Set SomeText -- set of words
  -> IntMap Char -- matched chars
  -> IntMap Char -- not matched chars
  -> Set Char-- other chars
  -> Set Char -- fired chars
  -> Set SomeText -- reduced set of words
updateWords wordSet matched notMatched other fired =
  Set.filter (predicate . fromSomeText) wordSet
  where
    predicate t@(mkIntMap -> im)
      =  hasMatched matched im
      && hasNotMatched notMatched im
      && hasOther other (Set.fromList $ T.unpack t)
      && hasFired fired (Set.fromList $ T.unpack t)



-- >>> im1 = IntMap.fromList [(2,'а'),(3,'л'),(5,'а')]
-- >>> s1 = Set.fromList "a"
-- >>> im2 = mkIntMap "палка"
-- >>> hasMatched im1 im2
-- False

-- не пускать слово, если список совпадений букв уже есть,
-- но в слове они не нашлись.
hasMatched :: IntMap Char -- matched
  -> IntMap Char -- word from set
  -> Bool
hasMatched (IntMap.null -> True) _ = True
hasMatched im1@(IntMap.keys -> s1) im2
  = go s1 True
  where
    go [] acc = acc
    go s acc =
      (IntMap.!) im1 (head s) == (IntMap.!) im2 (head s)
        && go (tail s) acc

hasNotMatched :: IntMap Char -- matched
  -> IntMap Char -- word from set
  -> Bool
hasNotMatched (IntMap.null -> True) _ = True
hasNotMatched im1@(IntMap.keys -> s1) im2
  = go s1 True
  where
    go [] acc = acc
    go s acc =
      (IntMap.!) im1 (head s) /= (IntMap.!) im2 (head s)
        && go (tail s) acc

hasOther :: Set Char -- other chars
  -> Set Char -- word from set
  -> Bool
hasOther (Set.null -> True) _ = True
hasOther sc ws = sc == Set.fromList (Set.foldl' helper [] ws)
  where
    helper acc x = if Set.member x sc then x : acc else acc

-- >>> hasOther (Set.fromList "палка") (Set.fromList "полк")
-- False


hasFired :: Set Char -- fired chars
  -> Set Char -- word from set
  -> Bool
hasFired sc ws =
  null sc || Set.null (Set.intersection sc ws)

mkIntMap :: Text -> IntMap Char
mkIntMap = IntMap.fromList . zip [1..] . T.unpack

checkIntersection :: IntMap Char -- riddle
  -> IntMap Char -- answer
  -> (IntMap Char, Set Char)
  -- ^ (matched char in place, matched char out of place, missing chars)
checkIntersection riddle =
    IntMap.foldlWithKey' (checker riddle) (IntMap.empty, Set.empty)

checker :: IntMap Char -- riddle
  -> (IntMap Char, Set Char) -- accumulator
  -> Int -- key of answer char
  -> Char -- char of answer
  -> (IntMap Char, Set Char)
checker riddle (i,m) k c = updateAcc k c (i,m)
  where
    updateAcc :: Int
      -> Char
      -> (IntMap Char, Set Char)
      -> (IntMap Char, Set Char)
    updateAcc k' c' (im, s) = case IntMap.lookup k' riddle of
      Just r -> if c' == r
        then (IntMap.insert k' c' im, s)
        else (im, filtered c' <> s)
      Nothing -> (im, s)

    filtered :: Char -> Set Char
    filtered c' = Set.filter (==c') $ Set.fromList $ IntMap.elems riddle

firedChars :: SomeText
  -> (IntMap Char, Set Char)
  -> Set Char
firedChars answer (matched, other) = Set.difference (Set.fromList $ T.unpack $ fromSomeText answer) $ Set.fromList (IntMap.elems matched) <> other


-- It prepares files with a list of a particular length
{-# DEPRECATED getWords "It not needed anymore. @hordle@ is enough " #-}
getWords :: FilePath -> FilePath -> Int -> IO ()
getWords input output limit = do
  bFile <- B.readFile input
  let allLines = Line.lines $ Line.fromText $ TE.decodeUtf8 bFile
  let striped = map T.strip allLines
  -- print striped
  let wordLimited =
        filter (\w -> T.length w == limit && T.any (/='-') w) striped
  B.writeFile output $ TE.encodeUtf8 $ T.unlines wordLimited

-- If you have made several attempts, you could try fetch list of left words. More data, less list.
-- Not matched chars should be also copied to other found chars.
fetchLeftWords :: Natural -- word length
  -> [(Int,Char)] -- matched chars
  -> [(Int,Char)] -- not matched chars
  -> String -- other found chars
  -> String -- fired chars
  -> IO ()
fetchLeftWords
  len
  (IntMap.fromList -> matched)
  (IntMap.fromList -> notMatched)
  (Set.fromList -> other)
  (Set.fromList -> fired) =  do
    bFile <- B.readFile file
    let allLines = Line.lines $ Line.fromText $ TE.decodeUtf8 bFile
    let setOfWords = getFixedWords len allLines
    -- let setOfWords = Set.fromList allLines
    let leftWords = updateWords
          setOfWords
          matched
          notMatched
          other
          fired

    mapM_ (TIO.putStrLn . fromSomeText) $ Set.toList leftWords

getFixedWords :: Natural
  -> [Text]
  -> Set SomeText
getFixedWords n = Set.fromList . mapMaybe (mkSomeText n)

