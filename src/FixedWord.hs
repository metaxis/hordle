{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE GADTs               #-}
{-# LANGUAGE NoStarIsType        #-}
{-# LANGUAGE PolyKinds           #-}
{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE ScopedTypeVariables #-}


module FixedWord
  (
    SomeText
  , fromSomeText
  , mkSomeText
  ) where

import Data.Proxy (Proxy)
import Data.Text (Text)
import qualified Data.Text as T
import Data.Type.Nat (Nat, SNat, SNatI, fromNatural, reify, snat, snatToNatural)
import Numeric.Natural (Natural)


data FixedText (n :: Nat)  where
  FixedText :: Text -> FixedText n
  deriving stock (Eq, Show, Ord)

fixText :: forall n . SNatI n
  => Text
  -> Maybe (FixedText n)
fixText (T.strip -> txt)
  | n == T.length txt = Just (FixedText txt)
  | otherwise         = Nothing
  where
    n = fromIntegral $ snatToNatural (snat :: SNat n)

fromSomeText :: SomeText -> Text
fromSomeText (MkSomeText (FixedText str)) = str

data SomeText where
  MkSomeText :: FixedText n -> SomeText

instance Show SomeText  where
  show = T.unpack . fromSomeText

instance Eq SomeText where
  MkSomeText (FixedText s1 :: FixedText n1) == (MkSomeText (FixedText s2 :: FixedText n2)) =
    s1 == s2

instance Ord SomeText where
  compare (MkSomeText (FixedText s1 :: FixedText n1)) (MkSomeText (FixedText s2 :: FixedText n2)) =
     compare s1 s2

mkSomeText :: Natural -> Text -> Maybe SomeText
mkSomeText n t =
  -- @reify@ promotes Nat to type level
  -- hence @SNatI n@ becomes defined at runtime
  reify (fromNatural n) (fmap MkSomeText . toSomeText)
  where
    toSomeText :: SNatI n => Proxy n -> Maybe (FixedText n)
    toSomeText _ = fixText t

-- >>> mkSomeText 5 "hello1"
-- Nothing

-- >>> mkSomeText 5 "hello"
-- Just hello
