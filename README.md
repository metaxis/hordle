# hordle

Functions to make **wordle** game easily to play.

## Instruction

- Make robot to play.
  1. Get full list of words of a language. One word per line from somewhere and save to app root directory.
  3. Set a riddle word and see how a robot guesses it with `hordle`.

- If you are a human and plays against a computer and already made several unlucky attempts, try `fetchLeftWords` to retrieve a list of all possible words are left.

- Currently dictionaries is set to Russian. For other languages feel free to update `singular.txt` with own dictionary.

