module Main (main) where

import Hordle (projectName)


main :: IO ()
main = putStrLn ("Benchmarks for " ++ projectName)
