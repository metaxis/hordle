module Main (main) where

import Hordle (hordle)
import System.Environment
import Data.Text (pack)


main :: IO ()
main = do
  arg <- getArgs
  hordle $ pack $ head arg

-- stack run -- "полка"
